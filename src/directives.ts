import { repeat } from 'lit-html/directives/repeat.js';
import { ifDefined } from 'lit-html/directives/if-defined.js';
import {html} from 'lit-element';
import {render} from 'lit-html';

const getId = ({ id }) => id;

const stooges = [
    { id: 1, name: 'Larry', img: 'https://dtlon6z3v1kfl.cloudfront.net/wp-content/uploads/2017/01/11092058/6803669.jpg' },
    { id: 2, name: 'Curly' },
    { id: 3, name: 'Moe', img: 'https://www.quotationof.com/images/moe-howards-quotes-1.jpg' }
];

const stoogeTpl = ({ id, name, img }) => html`
  <li data-stooge="${id}">
  <span>${name}</span>
    <img src="${ifDefined(img)}"/>
  </li>`;

const stoogesTpl = html`<ul>${repeat(stooges, getId, stoogeTpl)}</ul>`;
render(stoogesTpl, document.body)
