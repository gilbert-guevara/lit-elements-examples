// Import stylesheets
import './style.css';
import './cat';
import './counter'
import {LitElement, html, customElement} from "lit-element";

@customElement('counter-app')
export class CounterApp extends LitElement {
    constructor() {
        super();
        this.customValue = 5;
    }

    render() {
        return html`
      <web-counter
        @valueChange=${(e) => this.log(e)} 
        .value="${this.customValue}">
      </web-counter>
      <my-cat .alias="${["Flurry kitty"]}"></my-cat>
    `;
    }

    log(e) {
        console.log(e);
    }
}